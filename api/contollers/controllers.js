const { handleGetRequest } = require('../api/api');

const remoteBaseUrl = 'https://pixabay.com/api/?key=25540812-faf2b76d586c1787d2dd02736';

const fetchRemoteData = (req, res, next) => {
  
  let remoteUrl = remoteBaseUrl; // base URL with creds
  
  if (req.body) { // Assumed that page and per_Page params are required and exist in every request

    let queryStr = '';

    if (req.body.filter.length) { // if there is a filter => apply it via query params in fetch URL for pixabay
      queryStr = queryStr.concat(`&q=${req.body.filter}`);
    };

    queryStr = queryStr.concat(`&page=${req.body.page}`, `&per_page=${req.body.perPage}`);

    remoteUrl = remoteUrl.concat(queryStr);
  };
 
  handleGetRequest(req, res, next, remoteUrl);
};

const filterById = (req, res, next) => { // controller for id filter

  let remoteUrl = remoteBaseUrl.concat(`&${req.params.id}`);

  handleGetRequest(req, res, next, remoteUrl);

};

module.exports = {fetchRemoteData, filterById};