const handleGetRequest = (req, res, next, remoteUrl) => {
  return fetch(remoteUrl, {timeout: 20000}) // after several bugs in fluent work with the pixabay api and TimeOut errors add 'timeout' option
  .then(response => response.json())
  .then(data => {
    // Attach the fetched data to the request object
    res.locals.remoteData = data;
    next();
  })
  .catch(error => {
    // Handle any errors that occurred during the fetch
    console.error('Error fetching remote data:', error);
    res.status(500).send('Error fetching remote data');
  });
};

module.exports = { handleGetRequest };