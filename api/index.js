const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const port = 3001;
const app = express();

app.use(cors()); // avoid CORS errors 
app.use(bodyParser.json()); // use for parsing query params sent from the front

// import controllers and helpers
const { fetchRemoteData, filterById } = require('./contollers/controllers');
const { extractAndSendDataToClient } = require('./helpers/helper');

app.post('/', fetchRemoteData, extractAndSendDataToClient);

app.get('/:id', filterById, extractAndSendDataToClient);

app.listen(port, () => {
  console.log(`App listening on port: ${port}`)
});