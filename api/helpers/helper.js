const extractAndSendDataToClient = (req, res) => {
  const remoteData = res.locals.remoteData;
  // Send the fetched data to the client
  res.send(remoteData);
};

module.exports = { extractAndSendDataToClient };
