import React from 'react';
import './styles/App.css';
import { useSelector } from 'react-redux';
import store from './store/store';
import Loader from './components/Loader';
import PictureTable from './components/PicturesTable';
import { getData } from './store/actions';

function App() {

  const appState = useSelector((state) => state.hitsReducer);

  const handleClick = (evt) => { // handle "Next"/"Prev" calls to the api
    if (evt.target.id === 'prev' && appState.page === 1) return // avoid requests for pages with numbers less then 1
    switch (evt.target.id) {
      case 'next':
        store.dispatch(
          getData({
            ...appState,
            desired: 'next'
        }));
        break;
      case 'prev':
        store.dispatch(
          getData({
            ...appState,
            desired: 'prev'          
        }))
        break;
    };
  };

  const handleSelectFilter = (evt) => { // handle selection of filter
    store.dispatch(
      getData({
        ...appState,
        filter: evt.target.value
      }));
  };

  // possible options for tags
  const tagOptions = ['', 'sport', 'work', 'flower', 'nature', 'love', 'war', 'sea', 'sky'];

  return (
    <div className="App">
      {appState.loading ? <Loader /> : ''}
      <div className="App__main-screen">
        <div className="App__button-cont">
          <button id='prev' className="App__button" onClick={handleClick}>Prev</button>
          <select className="App__button" onChange={handleSelectFilter}>
            {tagOptions.map((option, index) => <option value={option} key={index}>{option}</option>)}
          </select>
          <button id='next' className="App__button" onClick={handleClick}>Next</button>
        </div>
        <PictureTable />
      </div>
    </div>
  );
}

export default App;
