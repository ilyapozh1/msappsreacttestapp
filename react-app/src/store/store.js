import { configureStore, applyMiddleware } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';

import { hitsReducer } from './reducers';

const store = configureStore({ // Initialize store
  reducer: {
    hitsReducer: hitsReducer
  },
}, applyMiddleware(thunk));

export default store;