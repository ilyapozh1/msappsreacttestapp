import * as types from './types';

export const initialState = {
  filter: '',
  page: 1,
  perPage: 9,
  hits: [],
  loading: false,
};

export const hitsReducer = (state = initialState, action) => {
  
  switch (action.type) {
    case types.GET_DATA_ATTEMPT:
      return { ...state, loading: true };
    case types.GET_DATA_SUCCESS:
      return { 
        ...state,
        hits: action.payload.hits,
        loading: false,
        page: action.payload.page,
        filter: action.payload.filter
      };
    case types.GET_DATA_FAIL:
      return { ...state, loading: false };
    default:
      // If the reducer doesn't care about action type,
      // return the existing state unchanged
      return state;
  }
};