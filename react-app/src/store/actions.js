import { initialState } from "./reducers";

const initState = {...initialState};

// Async action creator for GET API Route

export const getData = ({ 
  filter = initState.filter,
  page = initState.page,
  perPage = initState.perPage,
  desired = undefined,
}) => async (dispatch) => {
  
  dispatch({type: 'GET_DATA_ATTEMPT'});
  
  const URL = 'http://localhost:3001'; // backend local URL
  
  let queryParams = {
    filter,
    page,
    perPage,
    desired
  }
 
  switch (desired) { // decide what fetch params send to api
    case undefined:
      break;
    case 'next':
      queryParams.page = page + 1;
      break;
    case 'prev':
      queryParams.page = page - 1;
      break;
  }

  const body = await JSON.stringify({
    filter,
    page: queryParams.page,
    perPage
  });

  fetch(URL, {
    method: 'Post',
    headers: { 'Content-Type': 'application/json'},
    body: body,
  })
  .then(res => {
    return res.json();
  })
  .then(res => {
    switch (desired) {
      case undefined: 
        res.desired = undefined;
        break;
      case 'next':
        res.desired = 'next';
        break;
      case 'prev':
        res.desired = 'prev';
        break;
    };
    dispatch({
      type : 'GET_DATA_SUCCESS',
      payload: {
        ...res,
        page: queryParams.page,
        filter: filter
      }
    })
  })
  .catch(err => {
    dispatch({type: 'GET_DATA_FAIL'});
    console.log('API failed', err)
  })
}