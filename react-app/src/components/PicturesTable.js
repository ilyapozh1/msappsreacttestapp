import React from 'react';
import '../styles/picTable.css';
import { useSelector } from 'react-redux';
import PicItem from './PicItem';

const PictureTable = () => {

  const picDisplay = useSelector(state => state.hitsReducer.hits);

  return (
    <div className="table">
      {picDisplay.map(pic => {
        return <PicItem picInfo={pic} key={pic.id}/>
      })}
    </div>
  );
};

export default PictureTable;