import React from 'react';
import '../styles/picItem.css';

const PicItem = (props) => {

  // both of these state parameters are placed in local scope of the item 
  // because assumed that several modals can be opened
  const [openModal, setOpenModal] = React.useState(false);
  const [picInfo, setPicInfo] = React.useState([]); 

  const prepareInfoForRender = () => {
    let arr = [];
    for (const [key, value] of Object.entries(props.picInfo)) {
      arr.push(`${key}: ${value}`);
    };
    setPicInfo(arr);
  };

  const handleOpenModal = () => {
    setOpenModal(true);
    if (picInfo.length > 0) { // avoid rerender after first time opened
      return
    }
    prepareInfoForRender();
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  return (
    <div className='pic-cont'>
      {openModal ? 
        <div className='text-cont'>
          <button onClick={handleCloseModal}>Close info</button>
          {picInfo.map((picI, index) => {
            return <p key={index}>{picI}</p>
          })}
        </div> : 
        <img 
          onClick={handleOpenModal} 
          src={props.picInfo.largeImageURL} 
          alt='pic' 
          className='pic-item'>
        </img>
      }
    </div>
  );
};

export default PicItem;